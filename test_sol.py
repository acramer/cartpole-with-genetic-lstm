import gym
import pickle
import numpy as np
from cartpole import LSTM_Member

iters = 10000
member = LSTM_Member

with open('elite_sol.pickle','rb') as f:
    genotype = pickle.load(f)

elite = member(genotype)

env = gym.make('CartPole-v0')
obs = np.delete(env.reset(),1)

process = lambda x: (np.delete(x[0],1),x[1],x[2])
episode_done = False
fitness = 0
i = 0
#while not episode_done or i<iters:
reward = 1
while reward and i<iters:
    obs,reward,episode_done = process(env.step(elite.action(obs)))
    fitness += reward
    env.render()
    i += 1
    if i%1000 == 0: print('Fitness:',fitness)
env.close()
print('Fitness:',fitness)
