import gym
import torch
import torch.nn as nn
import numpy as np
from random import random

import sys
import argparse


def set_phenotype(phenotype, genotype):
    input_size = 3
    hidden_size = 2
    state = phenotype.state_dict()

    pt_gate_size = 4*hidden_size
    W_sizes = [pt_gate_size*input_size,\
               pt_gate_size*hidden_size,\
               pt_gate_size,\
               pt_gate_size]
    W_slices = [genotype[:W_sizes[0]],\
                genotype[W_sizes[0]:sum(W_sizes[:2])],\
                genotype[sum(W_sizes[:2]):sum(W_sizes[:3])],\
                genotype[sum(W_sizes[:3]):]]
    W_shapes = [(pt_gate_size,input_size),\
                (pt_gate_size,hidden_size),\
                pt_gate_size,\
                pt_gate_size]

    for i,W_name in enumerate(state):
        state[W_name][:] = torch.Tensor(np.reshape(W_slices[i],W_shapes[i]))


gen = np.array([i for i in range(56)])
phen = nn.LSTM(3,2)
print(phen.state_dict())
set_phenotype(phen,gen)
print('------------------')
print(phen.state_dict())
