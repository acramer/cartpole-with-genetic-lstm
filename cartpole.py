import gym
import torch
import torch.nn as nn
import numpy as np
from random import random

import sys
import argparse
import pickle

class LSTM_Member():
    def __init__(self, genotype=None):
        self.input_size = 3
        self.hidden_size = 2
        self.phenotype = nn.LSTM(self.input_size, self.hidden_size)
        self.genotype = genotype

        get_genotype = lambda x: np.concatenate([np.reshape(x[y].numpy(),[-1]) for y in x])
        if type(genotype) != type(None): self.set_phenotype(genotype)
        else: self.genotype = get_genotype(self.phenotype.state_dict())

        self.hidden = None

    # Takes genotype and modifies LSTM model
    def set_phenotype(self, genotype=None):
        if type(genotype) == type(None): genotype=self.genotype
        state = self.phenotype.state_dict()

        pt_gate_size = 4*self.hidden_size
        W_sizes = [pt_gate_size*self.input_size,\
                   pt_gate_size*self.hidden_size,\
                   pt_gate_size,\
                   pt_gate_size]
        W_slices = [genotype[:W_sizes[0]],\
                    genotype[W_sizes[0]:sum(W_sizes[:2])],\
                    genotype[sum(W_sizes[:2]):sum(W_sizes[:3])],\
                    genotype[sum(W_sizes[:3]):]]
        W_shapes = [(pt_gate_size,self.input_size),\
                    (pt_gate_size,self.hidden_size),\
                    pt_gate_size,\
                    pt_gate_size]

        for i,W_name in enumerate(state):
            state[W_name][:] = torch.Tensor(np.reshape(W_slices[i],W_shapes[i]))

    # Classifys move left or right
    def action(self, x):
        x = torch.reshape(torch.Tensor(x),[1,1,-1])

        if self.hidden == None: probs,self.hidden = self.phenotype(x)
        else: probs,self.hidden = self.phenotype(x,self.hidden)

        move = np.argmax(probs.detach().numpy().squeeze())
        return move

    # Clones this object
    def clone(self):
        return member(self.genotype)




class Population():
    def __init__(self, member_type,
            population_size = 50,
            num_survivors   =  5,
            num_children    = 43,
            mutation_prob   = 0.05,):

        if type(member_type) != type:
            raise Exception(f'EXCEPTION: population.member_type expects type {type} instead recieved type {type(member_type)}')
        if (num_survivors + num_children) > population_size:
            raise Exception(f'EXCEPTION: num_survivors and num_children must not exceed population_size -- n+n{num_survivors + num_children} p:{population_size}')

        self.population_size = population_size
        self.num_survivors   = num_survivors
        self.num_children    = num_children
        self.num_new_members = population_size - num_survivors - num_children
        self.mutation_prob   = mutation_prob

        self.member_type = member_type
        self.members = None
        self.fitness = None
        self.best_genes = member_type().genotype
        self.initialize_population()
        
    # Desc
    def initialize_population(self):
        self.members = [self.member_type() for _ in range(self.population_size)]
        self.fitness = np.zeros(self.population_size)
    
    # Desc
    def crossover(self,memA_idx,memB_idx):
        # Randomly choose which value comes from which member
        genoA = self.members[memA_idx].genotype
        genoB = self.members[memB_idx].genotype
    
        geno = np.zeros(len(genoA))
        for idx in range(len(geno)):
            geno[idx] = genoA[idx] if random() < 0.5 else genoB[idx]
    
        mem = self.member_type(geno)
        return mem
    
    # Desc
    def mutate(self,mem):
        # Randomly re-init parts of genotype
        geno = mem.genotype
        for idx in range(len(geno)):
            if random() < self.mutation_prob: geno[idx] = random()*2-1
    
        mem.set_phenotype()
        return mem

    def next_gen(self):
        # -- New Members --
        new_members = []
        # Children 
        total_fitness = sum(self.fitness)
        choice_dist_A = [f/total_fitness for f in self.fitness]

        for _ in range(self.num_children):
            # Pick Parents 
            choiceA_idx = np.random.choice(np.arange(0,self.population_size),p=choice_dist_A)

            # Pick a different parent
            total_fitness = sum(self.fitness) - self.fitness[choiceA_idx]
            choice_dist_B = [f/total_fitness for f in self.fitness]
            choice_dist_B[choiceA_idx] = 0
            choiceB_idx = np.random.choice(np.arange(0,self.population_size),p=choice_dist_B)

            # -- Crossover -- -- Mutate --
            new_members.append(self.mutate(self.crossover(choiceA_idx,choiceB_idx)))
        # New init members 
        new_members.extend([self.member_type() for _ in range(self.num_new_members)])

        # -- Remove unsucessful --
        survivors = []
        self.best_genes = self.members[np.argmax(self.fitness)].genotype
        print('\tBEST MEMBER FITNESS:',max(self.fitness))
        print('\tAVG MEMBER FITNESS:',np.mean(self.fitness))
        print('\tMED MEMBER FITNESS:',np.median(self.fitness))
        for _ in range(self.num_survivors):
            rem_idx = np.argmax(self.fitness)
            survivors.append(self.members[rem_idx])
            self.fitness = np.delete(self.fitness,rem_idx)
        self.members = survivors

        # -- Add new members --
        self.members.extend(new_members)
        if len(self.members)<self.population_size: raise Exception(f'EXCEPTION:size - {len(self.members)} | expects - {self.population_size}')
        self.fitness = np.zeros(self.population_size)




def _parse_args():
    parser = argparse.ArgumentParser(description='cartpole.py')
    parser.add_argument('-r', dest='render',  default=False, action='store_true', help='Render testing')
    args = parser.parse_args()
    return args


def main():
    # -- Genetic Hyper-parameters --
    POPULATION_SIZE = 50
    NUMBER_OF_EPISODES = 20
    NUMBER_OF_SURVIVORS = 5
    NUMBER_OF_CHILDREN = 43
    NUMBER_OF_NEW_MEMS = POPULATION_SIZE - NUMBER_OF_SURVIVORS - NUMBER_OF_CHILDREN
    MUTATION_PROB = 0.05
    assert NUMBER_OF_NEW_MEMS+NUMBER_OF_CHILDREN+NUMBER_OF_SURVIVORS<=POPULATION_SIZE

    # If episode length is to be extended
    NUM_ITERATIONS = 1000

    # Get runtime arguments
    FLAGS = _parse_args()
    RENDER = FLAGS.render

    # Initialize Population
    pop = Population(LSTM_Member,
                    population_size = POPULATION_SIZE,
                    num_survivors   = NUMBER_OF_SURVIVORS,
                    num_children    = NUMBER_OF_CHILDREN,
                    mutation_prob   = MUTATION_PROB)

    # Initialize the environment
    env = gym.make('CartPole-v0')
    
    # Removes the infos parameter from step and the X momentum observation from the observations and 
    for episode_idx in range(NUMBER_OF_EPISODES):
        print('Starting Episode '+str(episode_idx))

        # -- Compute Fitness --
        for pidx in range(pop.population_size):
            pop.fitness[pidx] = get_fitness(pop.members[pidx],env)

        pop.next_gen()

    env.close()

    with open('elite_sol.pickle','wb') as f:
        pickle.dump(pop.best_genes,f,pickle.HIGHEST_PROTOCOL)

# Compute Fitness for 1 member
def get_fitness(agent,env,render=False,num_iter=0,num_loops=5):
            process = lambda x: (np.delete(x[0],1),x[1],x[2])
            fitness = 0

            for idx in range(num_loops):
                obs = np.delete(env.reset(),1)
                assert len(obs) == 3

                episode_done = False
                step_iters = 0
                while not episode_done or step_iters < num_iter:
                    obs,reward,episode_done = process(env.step(agent.action(obs)))
                    assert len(obs) == 3

                    #print(env.step(agent.action(obs))
                    fitness += reward
                    
                    if render: env.render()
                    step_iters += 1

            return fitness/num_loops


if __name__ == '__main__': main()
